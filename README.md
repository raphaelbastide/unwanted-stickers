# Unwanted Stickers

![Unwanted stickers](unwanted-stickers.png)

## How to get it

For [Signal](https://signalstickers.com/pack/b9b48c7cf2d813d8088d009cd54cf1fa)*
For [Telegram](https://t.me/addstickers/unwantedstickers)  
On Mastodon, it has been added to the [post.lurk instance](https://post.lurk.org/@raphael/106132816270751240)

`*` Signal sticker packs [can’t be updated](https://github.com/signalapp/Signal-Desktop/issues/4489), any trick to solve this issue is welcome.

## Licence

[CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)
